package com.amaysim.amaysim.services;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.amaysim.amaysim.pojo.UserData;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.InputStream;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by peterrosenberg on 12/04/2017.
 */
@RunWith(AndroidJUnit4.class)
public class DataLoaderTest {
    @Test
    public void deserialize() throws Exception {
        InputStream inputStream = InstrumentationRegistry.getTargetContext()
                .getAssets().open(DataLoader.JSON_FILE);
        UserData userData = new Deserializer().deserialize(inputStream);

        // check two values each only in the test
        assertThat(userData.accounts.id, is("2593177"));
        assertThat(userData.accounts.attributes.emailAddress, is("test@gmail.com"));

        assertThat(userData.services.id, is("0468874507"));
        assertThat(userData.services.attributes.msn, is("0468874507"));

        assertThat(userData.subscriptions.id, is("0468874507-0"));
        assertThat(userData.subscriptions.attributes.includedDataBalance, is(52400));

        assertThat(userData.products.id, is("4000"));
        assertThat(userData.products.attributes.price, is(3990));
    }
}
