package com.amaysim.amaysim;

import android.content.ComponentName;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.InstrumentationRegistry.getTargetContext;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.assertNoUnverifiedIntents;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Created by peterrosenberg on 14/04/2017.
 */
@RunWith(AndroidJUnit4.class)
public class LoginActivityTest extends AmaysimTest {
    @Rule
    public IntentsTestRule<LoginActivity> mActivityRule
            = new IntentsTestRule<>(LoginActivity.class, true, false);

    @Test
    public void loginFail() {
        setSharedPreference(AmaysimActivity.PREF_KEY_LOGGED_IN, false);
        mActivityRule.launchActivity(null);

        String email = "test@gmail.com";
        String msn = "00000";

        onView(withId(R.id.email)).perform(typeText(email));
        onView(withId(R.id.msn)).perform(typeText(msn));
        onView(withId(R.id.msn)).perform(closeSoftKeyboard());
        onView(withId(R.id.email_sign_in_button)).perform(click());
        onView(allOf(withId(android.support.design.R.id.snackbar_text),
                withText(getInstrumentation().getTargetContext().getString(
                        R.string.wrong_credentials))))
                .check(matches(isDisplayed()));
        assertNoUnverifiedIntents();
    }

    @Test
    public void loginWithButton() throws Throwable {
        setSharedPreference(AmaysimActivity.PREF_KEY_LOGGED_IN, false);
        mActivityRule.launchActivity(null);

        String email = "test@gmail.com";
        String msn = "0468874507";

        onView(withId(R.id.email)).perform(typeText(email));
        onView(withId(R.id.msn)).perform(typeText(msn));
        onView(withId(R.id.msn)).perform(closeSoftKeyboard());
        onView(withId(R.id.email_sign_in_button)).perform(click());
        intended(hasComponent(new ComponentName(getTargetContext(), SplashActivity.class)));

        // await splash time
        Thread.sleep(SplashActivity.SPLASH_TIME);
    }

    @Test
    public void loginWithEnterKey() throws Throwable {
        setSharedPreference(AmaysimActivity.PREF_KEY_LOGGED_IN, false);
        mActivityRule.launchActivity(null);

        String email = "test@gmail.com";
        String msn = "0468874507";

        onView(withId(R.id.email)).perform(typeText(email));
        onView(withId(R.id.msn)).perform(typeText(msn));
        onView(withId(R.id.msn)).perform(pressImeActionButton());
        intended(hasComponent(new ComponentName(getTargetContext(), SplashActivity.class)));

        // await splash time
        Thread.sleep(SplashActivity.SPLASH_TIME);
    }
}