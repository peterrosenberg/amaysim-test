package com.amaysim.amaysim;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertTrue;

/**
 * Created by peterrosenberg on 14/04/2017.
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityTest extends AmaysimTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule
            = new ActivityTestRule<>(MainActivity.class, true, false);

    @Test
    public void emailShown() throws Throwable {
        setSharedPreference(AmaysimActivity.PREF_KEY_LOGGED_IN, true);
        mActivityRule.launchActivity(null);

        onView(withId(R.id.email)).check(matches(isDisplayed()));
        onView(withId(R.id.email)).check(matches(withText("test@gmail.com")));
    }

    @Test
    public void redirectToLogin() throws Throwable {
        setSharedPreference(AmaysimActivity.PREF_KEY_LOGGED_IN, false);
        mActivityRule.launchActivity(null);

        assertTrue(mActivityRule.getActivity().isFinishing());
    }

    @Test
    public void signOut() throws Throwable {
        setSharedPreference(AmaysimActivity.PREF_KEY_LOGGED_IN, true);
        mActivityRule.launchActivity(null);

        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(
                withText(getInstrumentation().getTargetContext()
                        .getString(R.string.action_sign_out)))
                .perform(click());
        assertTrue(mActivityRule.getActivity().isFinishing());
    }
}