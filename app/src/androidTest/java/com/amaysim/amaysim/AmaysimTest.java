package com.amaysim.amaysim;

import android.preference.PreferenceManager;
import android.support.test.InstrumentationRegistry;

/**
 * Base class for the tests.
 * Created by peterrosenberg on 14/04/2017.
 */
public abstract class AmaysimTest {

    protected void setSharedPreference(String key, boolean value) {
        PreferenceManager
                .getDefaultSharedPreferences(InstrumentationRegistry.getTargetContext())
                .edit()
                .putBoolean(key, value)
                .apply();
    }
}
