package com.amaysim.amaysim;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amaysim.amaysim.pojo.UserData;
import com.amaysim.amaysim.services.DataLoader;
import com.amaysim.amaysim.services.Deserializer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Used as the main screen.
 * Done as fragment to make it easier to enchance the app e.g. for tablets.
 * Created by peterrosenberg on 12/04/2017.
 */
public class MainActivityFragment extends Fragment {
    private static final String API_DATE_FORMAT = "yyyy-MM-dd";

    private UserData mUserData;

    private TextView mNameTv;
    private TextView mContactNoTv;
    private TextView mEmailTv;

    private TextView mIncludedDataBalanceTv;
    private TextView mSubscriptionExpiryTv;
    private TextView mAutoRenewalTv;

    private TextView mProductNameTv;
    private TextView mPriceTv;
    private TextView mUnlimitedText;
    private TextView mUnlimitedTalk;
    private TextView mUnlimitedInternationalText;
    private TextView mUnlimitedInternationalTalk;

    private TextView mMsnTv;
    private TextView mCreditTv;
    private TextView mExpiryTv;

    public MainActivityFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DataLoader dataLoader = new DataLoader(new Deserializer());
        dataLoader.loadData(new DataLoader.LoadListener() {
            @Override
            public void loadFinished(UserData userData) {
                mUserData = userData;
                // check if view is loaded already
                if (mNameTv != null) {
                    applyDataOnView();
                }
            }
        }, getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        mNameTv = (TextView) rootView.findViewById(R.id.name);
        mContactNoTv = (TextView) rootView.findViewById(R.id.contact_number);
        mEmailTv = (TextView) rootView.findViewById(R.id.email);

        mMsnTv = (TextView) rootView.findViewById(R.id.msn);
        mCreditTv = (TextView) rootView.findViewById(R.id.credit);
        mExpiryTv = (TextView) rootView.findViewById(R.id.expiry);

        mIncludedDataBalanceTv = (TextView) rootView.findViewById(R.id.included_data_balance);
        mSubscriptionExpiryTv = (TextView) rootView.findViewById(R.id.subscription_expiry);
        mAutoRenewalTv = (TextView) rootView.findViewById(R.id.auto_renewal);

        mProductNameTv = (TextView) rootView.findViewById(R.id.product_name);
        mPriceTv = (TextView) rootView.findViewById(R.id.price);
        mUnlimitedText = (TextView) rootView.findViewById(R.id.unlimited_text);
        mUnlimitedTalk = (TextView) rootView.findViewById(R.id.unlimited_talk);
        mUnlimitedInternationalText = (TextView) rootView.findViewById(
                R.id.unlimited_international_text);
        mUnlimitedInternationalTalk = (TextView) rootView.findViewById(
                R.id.unlimited_international_talk);

        // check if data is loaded already
        if (mUserData != null) {
            applyDataOnView();
        }
        return rootView;
    }

    private void applyDataOnView() {
        final StringBuilder buf = new StringBuilder();
        buf.append(mUserData.accounts.attributes.title);
        buf.append(" ");
        buf.append(mUserData.accounts.attributes.firstName);
        buf.append(" ");
        buf.append(mUserData.accounts.attributes.lastName);
        mNameTv.setText(buf.toString());
        mContactNoTv.setText(mUserData.accounts.attributes.contactNumber);
        mEmailTv.setText(mUserData.accounts.attributes.emailAddress);

        mMsnTv.setText(mUserData.services.attributes.msn);
        mCreditTv.setText(
                getDollarDisplayString(mUserData.services.attributes.credit));
        mExpiryTv.setText(convertToLocalDateFormat(mUserData.services.attributes.creditExpiry));

        mIncludedDataBalanceTv.setText(
                getGbDisplayString(mUserData.subscriptions.attributes.includedDataBalance));
        mSubscriptionExpiryTv.setText(
                convertToLocalDateFormat(mUserData.subscriptions.attributes.expiryDate));
        mAutoRenewalTv.setText(
                getBooleanDisplayString(mUserData.subscriptions.attributes.autoRenewal));

        mProductNameTv.setText(mUserData.products.attributes.name);
        mPriceTv.setText(getDollarDisplayString(mUserData.products.attributes.price));
        mUnlimitedText.setText(
                getBooleanDisplayString(mUserData.products.attributes.unlimitedText));
        mUnlimitedTalk.setText(
                getBooleanDisplayString(mUserData.products.attributes.unlimitedTalk));
        mUnlimitedInternationalText.setText(
                getBooleanDisplayString(mUserData.products.attributes.unlimitedInternationalText));
        mUnlimitedInternationalTalk.setText(
                getBooleanDisplayString(mUserData.products.attributes.unlimitedInternationalTalk));
    }

    private String getDollarDisplayString(double centValue) {
        return String.format(getString(R.string.dollar_value), centValue / 100);
    }

    private String getGbDisplayString(int mbValue) {
        return String.format(getString(R.string.gb_value), mbValue / 1024);
    }

    private String getBooleanDisplayString(boolean value) {
        return value ? getString(R.string.yes) : getString(R.string.no);
    }

    private String convertToLocalDateFormat(String apiFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(API_DATE_FORMAT, Locale.US);
        try {
            Date date = sdf.parse(apiFormat);
            java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(
                    getContext());
            return dateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            // log to error logging tool like Crashlytics
        }
        return null;
    }
}
