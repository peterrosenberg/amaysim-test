package com.amaysim.amaysim;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.amaysim.amaysim.pojo.UserData;
import com.amaysim.amaysim.services.DataLoader;
import com.amaysim.amaysim.services.Deserializer;

/**
 * The login screen of the app.
 * Created by peterrosenberg on 12/04/2017.
 */
public class LoginActivity extends AmaysimActivity {
    private EditText mEmailView;
    private EditText mMsnView;

    private UserData mUserData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mEmailView = (EditText) findViewById(R.id.email);
        mMsnView = (EditText) findViewById(R.id.msn);
        mMsnView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    signIn();
                    return true;
                }
                return false;
            }
        });

        View signInButton = findViewById(R.id.email_sign_in_button);
        signInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });

        DataLoader dataLoader = new DataLoader(new Deserializer());
        dataLoader.loadData(new DataLoader.LoadListener() {
            @Override
            public void loadFinished(UserData userData) {
                mUserData = userData;
            }
        }, this);
    }

    /**
     * check the credentials and sign the user in if they are correct
     */
    private void signIn() {
        if (mUserData == null) {
            Snackbar.make(findViewById(android.R.id.content),
                    R.string.error_occurred,
                    Snackbar.LENGTH_LONG).show();
            return;
        }

        final String email = mEmailView.getText().toString();
        final String msn = mMsnView.getText().toString();

        if (mUserData.accounts.attributes.emailAddress.equals(email)
                && mUserData.services.attributes.msn.equals(msn)) {
            setLoggedIn(true);
            Intent splashActivity = new Intent(this, SplashActivity.class);
            startActivity(splashActivity);
            finish();
        } else {
            Snackbar.make(findViewById(android.R.id.content),
                    R.string.wrong_credentials,
                    Snackbar.LENGTH_LONG).show();
        }
    }
}
