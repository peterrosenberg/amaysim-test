package com.amaysim.amaysim;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

/**
 * Base class of all activities to handle login.
 * Created by peterrosenberg on 12/04/2017.
 */
public abstract class AmaysimActivity extends AppCompatActivity {
    // non private for test access
    static final String PREF_KEY_LOGGED_IN = "LOGGED_IN";

    @Override
    protected void onResume() {
        super.onResume();
        if (!isLoggedIn() && !(this instanceof LoginActivity)) {
            Intent loginIntent = new Intent(this, LoginActivity.class);
            startActivity(loginIntent);
            finish();
        }
    }

    protected boolean isLoggedIn() {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        return prefs.getBoolean(PREF_KEY_LOGGED_IN, false);
    }

    protected void setLoggedIn(boolean loggedIn) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.edit().putBoolean(PREF_KEY_LOGGED_IN, loggedIn).apply();
    }
}
