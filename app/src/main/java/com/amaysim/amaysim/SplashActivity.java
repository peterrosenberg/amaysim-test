package com.amaysim.amaysim;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 * Show the splash screen for SPLASH_TIME milliseconds and than continues to MainActivity.
 * Created by peterrosenberg on 12/04/2017.
 */
public class SplashActivity extends AmaysimActivity {
    // non private for test access
    static final int SPLASH_TIME = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, SPLASH_TIME);
    }
}
