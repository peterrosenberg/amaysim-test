package com.amaysim.amaysim.pojo;

/**
 * Created by peterrosenberg on 12/04/2017.
 */
public class Subscriptions {
    public String id;
    public final Attributes attributes = new Attributes();
    public final Relationships relationships = new Relationships();

    public class Attributes {
        public Integer includedDataBalance;
        // other values skipped
        public String expiryDate;
        public boolean autoRenewal;
        public boolean primarySubscription;
    }

    public class Relationships {
        public final Product product = new Product();

        public class Product {
            public final Data data = new Data();
        }
    }
}
