package com.amaysim.amaysim.pojo;

/**
 * Created by peterrosenberg on 12/04/2017.
 */
public class Accounts {
    public static final String JSON_RELATIONSHIPS = "relationships";
    public static final String JSON_SERVICES = "services";
    public static final String JSON_LINKS = "links";
    public static final String JSON_RELATED = "related";

    public static final String JSON_TITLE = "title";
    public static final String JSON_FIRST_NAME = "first-name";
    public static final String JSON_LAST_NAME = "last-name";
    public static final String JSON_DATE_OF_BIRTH = "date-of-birth";
    public static final String JSON_CONTACT_NUMBER = "contact-number";
    public static final String JSON_EMAIL_ADDRESS = "email-address";
    public static final String JSON_EMAIL_ADDRESS_VERIFIED = "email-address-verified";
    public static final String JSON_EMAIL_SUBSCRIPTION_STATUS = "email-subscription-status";

    public String id;
    public final Attributes attributes = new Attributes();
    public final Relationships relationships = new Relationships();

    public class Attributes {
        public String title;
        public String firstName;
        public String lastName;
        public String dateOfBirth;
        public String contactNumber;
        public String emailAddress;
        public boolean emailAddressVerified;
        public boolean emailSubscriptionStatus;
    }

    public class Relationships {
        public final Services services = new Services();

        public class Services {
            public String id;

            public final Links links = new Links();
        }
    }
}
