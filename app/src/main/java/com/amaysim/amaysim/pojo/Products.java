package com.amaysim.amaysim.pojo;

/**
 * Created by peterrosenberg on 12/04/2017.
 */
public class Products {
    public String id;
    public final Attributes attributes = new Attributes();

    public class Attributes {
        public String name;
        // other values skipped
        public boolean unlimitedText;
        public boolean unlimitedTalk;
        public boolean unlimitedInternationalText;
        public boolean unlimitedInternationalTalk;
        public int price;
    }
}
