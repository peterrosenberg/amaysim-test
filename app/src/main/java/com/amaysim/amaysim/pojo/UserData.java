package com.amaysim.amaysim.pojo;

/**
 * Created by peterrosenberg on 12/04/2017.
 */
public class UserData {
    public Accounts accounts;
    public Services services;
    public Subscriptions subscriptions;
    public Products products;
}
