package com.amaysim.amaysim.pojo;

/**
 * Created by peterrosenberg on 12/04/2017.
 */
public class Services {
    public String id;
    public final Attributes attributes = new Attributes();
    public final Relationships relationships = new Relationships();

    public class Attributes {
        public String msn;
        public int credit;
        public String creditExpiry;
        public boolean dataUsageThreshold;
    }

    public class Relationships {
        public final Subscriptions subscriptions = new Subscriptions();

        public class Subscriptions {
            public final Data data = new Data();
            public final Links links = new Links();

        }
    }
}
