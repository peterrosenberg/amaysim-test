package com.amaysim.amaysim.services;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.amaysim.amaysim.pojo.UserData;

import java.io.IOException;
import java.io.InputStream;

/**
 * Loads the data on a background thread.
 * Created by peterrosenberg on 12/04/2017.
 */
public class DataLoader {
    // non private for test access
    static final String JSON_FILE = "collection.json";
    private final Deserializer mDeserializer;

    /**
     * @param deserializer to use to load the data
     */
    public DataLoader(@NonNull Deserializer deserializer) {
        mDeserializer = deserializer;
    }

    /**
     * To be called on the UI thread. Will do the work on the background thread and
     * then do the callback on the UI thread afterwards.
     *
     * @param listener to call back when done
     * @param context  to use
     */
    public void loadData(@Nullable final LoadListener listener, @NonNull final Context context) {
        new AsyncTask<Void, Void, UserData>() {
            @Override
            protected UserData doInBackground(Void... params) {
                try {
                    InputStream inputStream = context.getAssets().open(DataLoader.JSON_FILE);
                    return mDeserializer.deserialize(inputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                    // log to error logging tool like Crashlytics
                }
                return null;
            }

            @Override
            protected void onPostExecute(UserData userData) {
                if (listener != null) {
                    listener.loadFinished(userData);
                }
            }
        }.execute();
    }

    public interface LoadListener {
        void loadFinished(UserData userData);
    }
}
