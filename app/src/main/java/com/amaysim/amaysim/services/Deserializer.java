package com.amaysim.amaysim.services;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.amaysim.amaysim.pojo.Accounts;
import com.amaysim.amaysim.pojo.Products;
import com.amaysim.amaysim.pojo.Services;
import com.amaysim.amaysim.pojo.Subscriptions;
import com.amaysim.amaysim.pojo.UserData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Deserializes the json data. In a production app, that would be done using json where
 * almost all of the code below is not needed. It uses annotations to fill up the
 * pojo objects.
 * Created by peterrosenberg on 12/04/2017.
 */
public class Deserializer {
    private static final String JSON_ACCOUNTS = "accounts";
    private static final String JSON_DATA = "data";
    private static final String JSON_ATTRIBUTES = "attributes";
    private static final String JSON_ATTR_TYPE = "type";
    private static final String JSON_INCLUDED = "included";
    private static final String JSON_SERVICES = "services";
    private static final String JSON_ID = "id";

    /**
     * Deserialize the data in inputStream on the same thread.
     *
     * @param inputStream to read from
     * @return the deserialized objects
     */
    public UserData deserialize(@NonNull InputStream inputStream) {
        UserData userData = new UserData();
        try {
            // would normally use gson library where stream parsing is done
            String jsonStr = convertStreamToString(inputStream);
            JSONObject jsonObj = new JSONObject(jsonStr);
            final JSONObject data = jsonObj.getJSONObject(JSON_DATA);
            if (JSON_ACCOUNTS.equals(data.getString(JSON_ATTR_TYPE))) {
                final Accounts accounts = new Accounts();
                accounts.id = data.getString(JSON_ID);
                final JSONObject attributes = data.getJSONObject(JSON_ATTRIBUTES);
                accounts.attributes.title = attributes.getString(Accounts.JSON_TITLE);
                accounts.attributes.firstName = attributes.getString(Accounts.JSON_FIRST_NAME);
                accounts.attributes.lastName = attributes.getString(Accounts.JSON_LAST_NAME);
                accounts.attributes.dateOfBirth = attributes.getString(
                        Accounts.JSON_DATE_OF_BIRTH);
                accounts.attributes.contactNumber = attributes.getString(
                        Accounts.JSON_CONTACT_NUMBER);
                accounts.attributes.emailAddress = attributes.getString(
                        Accounts.JSON_EMAIL_ADDRESS);
                accounts.attributes.emailAddressVerified = attributes.getBoolean(
                        Accounts.JSON_EMAIL_ADDRESS_VERIFIED);
                accounts.attributes.emailSubscriptionStatus = attributes.getBoolean(
                        Accounts.JSON_EMAIL_SUBSCRIPTION_STATUS);

                final JSONObject relationships = data.getJSONObject(Accounts.JSON_RELATIONSHIPS);
                final JSONObject services = relationships.getJSONObject(Accounts.JSON_SERVICES);
                final JSONObject links = services.getJSONObject(Accounts.JSON_LINKS);
                final String relatedStr = links.getString(Accounts.JSON_RELATED);

                accounts.relationships.services.links.related = Uri.parse(relatedStr);
                accounts.relationships.services.id = accounts.relationships.services.links.related.getLastPathSegment();
                userData.accounts = accounts;
            }

            final JSONArray includedArray = jsonObj.getJSONArray(JSON_INCLUDED);
            for (int i = 0; i < includedArray.length(); i++) {
                final JSONObject included = includedArray.getJSONObject(i);
                if (JSON_SERVICES.equals(included.getString(JSON_ATTR_TYPE))) {
                    final Services services = new Services();
                    services.id = included.getString(JSON_ID);
                    final JSONObject attributes = included.getJSONObject(JSON_ATTRIBUTES);
                    services.attributes.msn = attributes.getString("msn");
                    services.attributes.credit = attributes.getInt("credit");
                    services.attributes.creditExpiry = attributes.getString("credit-expiry");
                    services.attributes.dataUsageThreshold = attributes.getBoolean(
                            "data-usage-threshold");

                    final JSONObject relationships = included.getJSONObject(
                            Accounts.JSON_RELATIONSHIPS);
                    final JSONObject subscriptions = relationships.getJSONObject("subscriptions");
                    final JSONObject links = subscriptions.getJSONObject(Accounts.JSON_LINKS);
                    final String relatedStr = links.getString(Accounts.JSON_RELATED);
                    services.relationships.subscriptions.links.related = Uri.parse(relatedStr);

                    final JSONArray relationshipDataArray = subscriptions.getJSONArray("data");
                    for (int j = 0; j < relationshipDataArray.length(); j++) {
                        final JSONObject relationshipData = relationshipDataArray.getJSONObject(j);
                        if ("subscriptions".equals(relationshipData.getString("type"))) {
                            // assume there is only one entry with the same type in the json list
                            services.relationships.subscriptions.data.id = relationshipData.getString(
                                    JSON_ID);
                        }
                    }
                    userData.services = services;
                } else if ("subscriptions".equals(included.getString("type"))) {
                    Subscriptions subscriptions = new Subscriptions();
                    subscriptions.id = included.getString(JSON_ID);
                    final JSONObject attributes = included.getJSONObject("attributes");
                    subscriptions.attributes.includedDataBalance = attributes.getInt(
                            "included-data-balance");
                    subscriptions.attributes.expiryDate = attributes.getString("expiry-date");
                    subscriptions.attributes.autoRenewal = attributes.getBoolean("auto-renewal");
                    subscriptions.attributes.primarySubscription = attributes.getBoolean(
                            "primary-subscription");

                    final JSONObject relationships = included.getJSONObject("relationships");
                    final JSONObject product = relationships.getJSONObject("product");
                    final JSONObject productData = product.getJSONObject("data");
                    if ("products".equals(productData.getString("type"))) {
                        subscriptions.relationships.product.data.id = productData.getString(
                                JSON_ID);
                    }
                    userData.subscriptions = subscriptions;
                } else if ("products".equals(included.getString("type"))) {
                    Products products = new Products();
                    products.id = included.getString(JSON_ID);
                    final JSONObject attributes = included.getJSONObject("attributes");
                    products.attributes.name = attributes.getString("name");
                    products.attributes.unlimitedText = attributes.getBoolean("unlimited-text");
                    products.attributes.unlimitedTalk = attributes.getBoolean("unlimited-talk");
                    products.attributes.unlimitedInternationalText = attributes.getBoolean(
                            "unlimited-international-text");
                    products.attributes.unlimitedInternationalTalk = attributes.getBoolean(
                            "unlimited-international-talk");
                    products.attributes.price = attributes.getInt("price");
                    userData.products = products;
                }
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            // log to error logging tool like Crashlytics
        }
        return userData;
    }

    /**
     * Reads an input stream and return its content as a string.
     * Non private for test access
     *
     * @param is to read from
     * @return the string read from is of throws an IOException
     * @throws IOException when is cannot be read properly
     */
    String convertStreamToString(@NonNull InputStream is) throws IOException {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            return sb.toString();
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }
}
